#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <pthread.h>

#define SHM_SIZE 80

// Calculate the factorial of a number
int factorial(int n) {
    if (n == 0) {
        return 1;
    } else {
        int temp = n;
        for (int i = temp-1; i > 1; i--)
        {
            temp *= i;
        }
        return temp;
    };
}

// Thread function to calculate factorial for each element of the matrix
void* calculateFactorial(void* arg) {
    int (*matrix)[5] = (int (*)[5]) arg;

    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 5; j++) {
            matrix[i][j] = factorial(matrix[i][j]);
        }
    }

    pthread_exit(NULL);
}

int main() {
    // Get key for shared memory segment
    key_t key = ftok(".", 'x');
    if (key == -1) {
        perror("ftok");
        exit(1);
    }

    // Attach to shared memory segment
    int shmid = shmget(key, SHM_SIZE, 0666);
    if (shmid == -1) {
        perror("shmget");
        exit(1);
    };
    int (*shm_ptr)[5] = shmat(shmid, NULL, 0);
    if (shm_ptr == (int (*)[5]) -1) {
        perror("shmat");
        exit(1);
    };

    // Access shared memory matrix
    printf("Shared memory matrix:\n");
    int i, j;
    for (i = 0; i < 4; i++) {
        for (j = 0; j < 5; j++) {
            printf("%d ", shm_ptr[i][j]);
        }
        printf("\n");
    };

    // allocate memory for local matrix
    int (*matrix)[5] = malloc(sizeof(int[4][5]));

    // copy shared memory matrix to local matrix
    for (i = 0; i < 4; i++) {
        for (j = 0; j < 5; j++) {
            matrix[i][j] = shm_ptr[i][j];
        }
    };

    // Create thread to calculate factorial for each element of the matrix
    pthread_t thread_id;
    if (pthread_create(&thread_id, NULL, calculateFactorial, matrix) != 0) {
        perror("pthread_create");
        exit(1);
    }

    // Wait for thread to finish
    if (pthread_join(thread_id, NULL) != 0) {
        perror("pthread_join");
        exit(1);
    }

    // Print result matrix
    printf("Matrix after factorial calculation:\n");
    for (i = 0; i < 4; i++) {
        for (j = 0; j < 5; j++) {
            printf("%d ", matrix[i][j]);
        }
        printf("\n");
    };

    // Detach from shared memory segment
    if (shmdt(shm_ptr) == -1) {
        perror("shmdt");
        exit(1);
    };

    return 0;
}
