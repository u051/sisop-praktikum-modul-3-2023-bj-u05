#include<stdio.h>
#include<unistd.h>
#include<sys/shm.h>
#include<stdlib.h>
#include<time.h>
#include<sys/wait.h>

#define SHM_SIZE sizeof(int[4][5])

// Function to create 4x2 matrix with random values 1-5 (inclusive)
void createMatrix42(int matrix[4][2]) {
    int i, j;
    srand(time(NULL));
    for (i = 0; i < 4; i++) {
        for (j = 0; j < 2; j++) {
            matrix[i][j] = rand() % 5 + 1;
        }
    }
};

// Function to create 2x5 matrix with random values 1-4 (inclusive)
void createMatrix25(int matrix[2][5]) {
    int i, j;
    srand(time(NULL));
    for (i = 0; i < 2; i++) {
        for (j = 0; j < 5; j++) {
            matrix[i][j] = rand() % 4 + 1;
        }
    }
};

// Function to multiply 2 matrices
void multiplyMatrix(int matrix1[4][2], int matrix2[2][5], int matrix3[4][5]) {
    int i, j, k;
    for (i = 0; i < 4; i++) {
        for (j = 0; j < 5; j++) {
            matrix3[i][j] = 0;
            for (k = 0; k < 2; k++) {
                matrix3[i][j] += matrix1[i][k] * matrix2[k][j];
            }
        }
    }
};

int main() {

    // Create 4x2 matrix
    int matrix1[4][2];
    createMatrix42(matrix1);
    // Create 2x5 matrix
    int matrix2[2][5];
    createMatrix25(matrix2);

    // Result matrix
    int matrix3[4][5];
    multiplyMatrix(matrix1, matrix2, matrix3);

    // print matrix1
    printf("Matrix 1:\n");
    int i, j;
    for (i = 0; i < 4; i++) {
        for (j = 0; j < 2; j++) {
            printf("%d ", matrix1[i][j]);
        }
        printf("\n");
    };
    printf("\n");
    // print matrix2
    printf("Matrix 2:\n");
    for (i = 0; i < 2; i++) {
        for (j = 0; j < 5; j++) {
            printf("%d ", matrix2[i][j]);
        }
        printf("\n");
    };
    printf("\n");
    // Print result matrix
    printf("Matrix 3:\n");
    for (i = 0; i < 4; i++) {
        for (j = 0; j < 5; j++) {
            printf("%d ", matrix3[i][j]);
        }
        printf("\n");
    };

    // Create shared memory segment
    key_t key = ftok(".", 'x');
    int shmid = shmget(key, SHM_SIZE, IPC_CREAT | 0666);
    if (shmid < 0) {
        perror("shmget");
        exit(1);
    }

    // Attach shared memory segment to pointer
    int (*shm_ptr)[5] = shmat(shmid, NULL, 0);
    if (shm_ptr == (int (*)[5]) -1) {
        perror("shmat");
        exit(1);
    }

    // Copy matrix3 to shared memory
    for (i = 0; i < 4; i++) {
        for (j = 0; j < 5; j++) {
            shm_ptr[i][j] = matrix3[i][j];
        }
    }

    // Detach shared memory
    if (shmdt(shm_ptr) == -1) {
        perror("shmdt");
        exit(1);
    }

    return 0;
}