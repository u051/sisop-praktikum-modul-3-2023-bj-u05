# sisop-praktikum-modul-modul 1-2023-BJ-U05

## Group U05

Abiansyah Adzani Gymnastiar (5025211077)
I Putu Arya Prawira Wiwekananda (5025211065)
Satria Surya Prana (5025211073)

## Soal 1
1. Lord Maharaja Baginda El Capitano Harry Maguire, S.Or., S.Kom yang dulunya seorang pemain bola, sekarang sudah pensiun dini karena sering blunder dan merugikan timnya. Ia memutuskan berhenti dan beralih profesi menjadi seorang programmer. Layaknya bola di permainan sepak bola yang perlu dikelola, pada dunia programming pun perlu adanya pengelolaan memori. Maguire berpikir bahwa semakin kecil file akan semakin mudah untuk dikelola dan transfer file-nya juga semakin cepat dan mudah. Dia lantas menemukan Algoritma Huffman untuk proses kompresi lossless. Dengan kepintaran yang pas-pasan dan berbekal modul Sisop, dia berpikir membuat program untuk mengkompres sebuah file. Namun, dia tidak mampu mengerjakannya sendirian, maka bantulah Maguire membuat program tersebut! 
- Pada parent process, baca file yang akan dikompresi dan hitung frekuensi kemunculan huruf pada file tersebut. Kirim hasil perhitungan frekuensi tiap huruf ke child process.
- Pada child process, lakukan kompresi file dengan menggunakan algoritma Huffman berdasarkan jumlah frekuensi setiap huruf yang telah diterima.
- Kemudian (pada child process), simpan Huffman tree pada file terkompresi. Untuk setiap huruf pada file, ubah karakter tersebut menjadi kode Huffman dan kirimkan kode tersebut ke program dekompresi menggunakan pipe.
- Kirim hasil kompresi ke parent process. Lalu, di parent process baca Huffman tree dari file terkompresi. Baca kode Huffman dan lakukan dekompresi. 
- Di parent process, hitung jumlah bit setelah dilakukan kompresi menggunakan algoritma Huffman dan tampilkan pada layar perbandingan jumlah bit antara setelah dan sebelum dikompres dengan algoritma Huffman.
- Pada encoding ASCII, setiap karakter diwakili oleh 8 bit atau 1 byte, yang cukup untuk merepresentasikan 256 karakter yang berbeda
- Untuk karakter selain huruf tidak masuk ke perhitungan jumlah bit dan tidak perlu dihitung frekuensi kemunculannya
- Agar lebih mudah, ubah semua huruf kecil ke huruf kapital

### Pengerjaan dan Pembahasan
```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define MAX_CHAR 256

// Struktur untuk node Huffman Tree
struct MinHeapNode {
    char data;
    unsigned frequency;
    struct MinHeapNode *left, *right;
};

// Struktur untuk Min Heap
struct MinHeap {
    unsigned size;
    unsigned capacity;
    struct MinHeapNode** array;
};

// Membuat node baru pada Huffman Tree
struct MinHeapNode* newNode(char data, unsigned frequency) {
    struct MinHeapNode* node = (struct MinHeapNode*)malloc(sizeof(struct MinHeapNode));
    node->left = node->right = NULL;
    node->data = data;
    node->frequency = frequency;
    return node;
}

// Membuat Min Heap kosong
struct MinHeap* createMinHeap(unsigned capacity) {
    struct MinHeap* minHeap = (struct MinHeap*)malloc(sizeof(struct MinHeap));
    minHeap->size = 0;
    minHeap->capacity = capacity;
    minHeap->array = (struct MinHeapNode**)malloc(minHeap->capacity * sizeof(struct MinHeapNode*));
    return minHeap;
}

// Menukar dua node pada Min Heap (digunakan dalam heapify)
void swapMinHeapNode(struct MinHeapNode** a, struct MinHeapNode** b) {
    struct MinHeapNode* t = *a;
    *a = *b;
    *b = t;
}

// Melakukan penyesuaian ke bawah (heapify) pada subpohon dengan root i
void minHeapify(struct MinHeap* minHeap, int i) {
    int smallest = i;
    int left = 2 * i + 1;
    int right = 2 * i + 2;

    if (left < minHeap->size && minHeap->array[left]->frequency < minHeap->array[smallest]->frequency)
        smallest = left;

    if (right < minHeap->size && minHeap->array[right]->frequency < minHeap->array[smallest]->frequency)
        smallest = right;

    if (smallest != i) {
        swapMinHeapNode(&minHeap->array[i], &minHeap->array[smallest]);
        minHeapify(minHeap, smallest);
    }
}

// Cek apakah ukuran heap hanya 1
int isSizeOne(struct MinHeap* minHeap) {
    return (minHeap->size == 1);
}

// Mengeluarkan node dengan frekuensi terkecil dari min heap
struct MinHeapNode* extractMin(struct MinHeap* minHeap) {
    struct MinHeapNode* temp = minHeap->array[0];
    minHeap->array[0] = minHeap->array[minHeap->size - 1];
    --minHeap->size;
    minHeapify(minHeap, 0);
    return temp;
}

// Memasukkan node ke min heap
void insertMinHeap(struct MinHeap* minHeap, struct MinHeapNode* minHeapNode) {
    ++minHeap->size;
    int i = minHeap->size - 1;
    while (i && minHeapNode->frequency < minHeap->array[(i - 1) / 2]->frequency) {
        minHeap->array[i] = minHeap->array[(i - 1) / 2];
        i = (i - 1) / 2;
    }
    minHeap->array[i] = minHeapNode;
}
```

## Soal 2
2. Fajar sedang sad karena tidak lulus dalam mata kuliah Aljabar Linear. Selain itu, dia tambah sedih lagi karena bertemu matematika di kuliah Sistem Operasi seperti kalian 🥲. Karena dia tidak bisa ngoding dan tidak bisa menghitung, jadi Fajar memohon jokian kalian. Tugas Fajar adalah sebagai berikut.
- Membuat program C dengan nama kalian.c, yang berisi program untuk melakukan perkalian matriks. Ukuran matriks pertama adalah 4×2 dan matriks kedua 2×5. Isi dari matriks didefinisikan di dalam code. Matriks nantinya akan berisi angka random dengan rentang pada matriks pertama adalah 1-5 (inklusif), dan rentang pada matriks kedua adalah 1-4 (inklusif). Tampilkan matriks hasil perkalian tadi ke layar.
- Buatlah program C kedua dengan nama cinta.c. Program ini akan mengambil variabel hasil perkalian matriks dari program kalian.c (program sebelumnya). Tampilkan hasil matriks tersebut ke layar. 
(Catatan: wajib menerapkan konsep shared memory)
- Setelah ditampilkan, berikutnya untuk setiap angka dari matriks tersebut, carilah nilai faktorialnya. Tampilkan hasilnya ke layar dengan format seperti matriks.
Contoh: 
array [[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12], ...],<br>
maka: <br>
1 2 6 24 120 720 ... ... …
(Catatan: Wajib menerapkan thread dan multithreading dalam penghitungan faktorial)
- Buatlah program C ketiga dengan nama sisop.c. Pada program ini, lakukan apa yang telah kamu lakukan seperti pada cinta.c namun tanpa menggunakan thread dan multithreading. Buat dan modifikasi sedemikian rupa sehingga kamu bisa menunjukkan perbedaan atau perbandingan (hasil dan performa) antara program dengan multithread dengan yang tidak. 
Dokumentasikan dan sampaikan saat demo dan laporan resmi.

### Pengerjaan dan Pembahasan
- dikerjakan oleh: Abiansyah Adzani Gymnastiar (5025211077)
- Code untuk poin 1 :<br>
pada pembuatan matrix menggunakan fungsi sebagai berikut
``` C
void createMatrix42(int matrix[4][2]) {
    int i, j;
    srand(time(NULL));
    for (i = 0; i < 4; i++) {
        for (j = 0; j < 2; j++) {
            matrix[i][j] = rand() % 5 + 1;
        }
    
```
- untuk perkalian, fungsi sebagai berikut
``` C
void multiplyMatrix(int matrix1[4][2], int matrix2[2][5], int matrix3[4][5]) {
    int i, j, k;
    for (i = 0; i < 4; i++) {
        for (j = 0; j < 5; j++) {
            matrix3[i][j] = 0;
            for (k = 0; k < 2; k++) {
                matrix3[i][j] += matrix1[i][k] * matrix2[k][j];
            }
        }
    }
};
```
- Code untuk point 2 dan 3 : <br>
pada poin ini kita membuat shared memory untuk mengakses matrix pada kalian.c. Lalu, dapat menemukan hasil faktorial dengan menggunakan 2 funsi berikut
``` C
// Calculate the factorial of a number
int factorial(int n) {
    if (n == 0) {
        return 1;
    } else {
        int temp = n;
        for (int i = temp-1; i > 1; i--)
        {
            temp *= i;
        }
        return temp;
    };
}

// Thread function to calculate factorial for each element of the matrix
void* calculateFactorial(void* arg) {
    int (*matrix)[5] = (int (*)[5]) arg;

    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 5; j++) {
            matrix[i][j] = factorial(matrix[i][j]);
        }
    }

    pthread_exit(NULL);
}
```
- setelah itu, akses variable matrix dan copy kedalam local matrix untuk mem-faktorialkan matrix
- lalu, buat thread untuk melakukan faktorial. Code dapat dilihat sebagai berikut:
``` C
    // Access shared memory matrix
    printf("Shared memory matrix:\n");
    int i, j;
    for (i = 0; i < 4; i++) {
        for (j = 0; j < 5; j++) {
            printf("%d ", shm_ptr[i][j]);
        }
        printf("\n");
    };

    // allocate memory for local matrix
    int (*matrix)[5] = malloc(sizeof(int[4][5]));

    // copy shared memory matrix to local matrix
    for (i = 0; i < 4; i++) {
        for (j = 0; j < 5; j++) {
            matrix[i][j] = shm_ptr[i][j];
        }
    };

    // Create thread to calculate factorial for each element of the matrix
    pthread_t thread_id;
    if (pthread_create(&thread_id, NULL, calculateFactorial, matrix) != 0) {
        perror("pthread_create");
        exit(1);
    }

    // Wait for thread to finish
    if (pthread_join(thread_id, NULL) != 0) {
        perror("pthread_join");
        exit(1);
    }

    // Print result matrix
    printf("Matrix after factorial calculation:\n");
    for (i = 0; i < 4; i++) {
        for (j = 0; j < 5; j++) {
            printf("%d ", matrix[i][j]);
        }
        printf("\n");
    };

    // Detach from shared memory segment
    if (shmdt(shm_ptr) == -1) {
        perror("shmdt");
        exit(1);
    };
```
- Code untuk poin 4 <br>
Menggunakan sistem shared memory yang sama dengan cinta.c, namun tidak menggunakan threading. Code dapat dilihat seperti berikut:
``` C
    // calculate factorial for each element of the matrix
    calculateFactorial(matrix);

    // print matrix
    printf("Matrix after factorial calculation:\n");
    for (i = 0; i < 4; i++) {
        for (j = 0; j < 5; j++) {
            printf("%d ", matrix[i][j]);
        }
        printf("\n");
    };
```
- dengan fungsi faktorial seperti berikut:
```
// Calculate the factorial of a number
int factorial(int n) {
    if (n == 0) {
        return 1;
    } else {
        int temp = n;
        for (int i = temp-1; i > 1; i--)
        {
            temp *= i;
        }
        return temp;
    };
}

// function to calculate factorial for each element of the matrix
void calculateFactorial(int (*matrix)[5]) {
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 5; j++) {
            matrix[i][j] = factorial(matrix[i][j]);
        }
    }
}

```
## Soal 4
4. Suatu hari, Amin, seorang mahasiswa Informatika mendapati suatu file bernama hehe.zip. Di dalam file .zip tersebut, terdapat sebuah folder bernama files dan file .txt bernama extensions.txt dan max.txt. Setelah melamun beberapa saat, Amin mendapatkan ide untuk merepotkan kalian dengan file tersebut! 
Download dan unzip file tersebut dalam kode c bernama unzip.c.
Selanjutnya, buatlah program categorize.c untuk mengumpulkan (move / copy) file sesuai extension-nya. Extension yang ingin dikumpulkan terdapat dalam file extensions.txt. Buatlah folder categorized dan di dalamnya setiap extension akan dibuatkan folder dengan nama sesuai nama extension-nya dengan nama folder semua lowercase. Akan tetapi, file bisa saja tidak semua lowercase. File lain dengan extension selain yang terdapat dalam .txt files tersebut akan dimasukkan ke folder other.
Pada file max.txt, terdapat angka yang merupakan isi maksimum dari folder tiap extension kecuali folder other. Sehingga, jika penuh, buatlah folder baru dengan format extension (2), extension (3), dan seterusnya.
Output-kan pada terminal banyaknya file tiap extension terurut ascending dengan semua lowercase, beserta other juga dengan format sebagai berikut.
extension_a : banyak_file
extension_b : banyak_file
extension_c : banyak_file
other : banyak_file
Setiap pengaksesan folder, sub-folder, dan semua folder pada program categorize.c wajib menggunakan multithreading. Jika tidak menggunakan akan ada pengurangan nilai.
Dalam setiap pengaksesan folder, pemindahan file, pembuatan folder pada program categorize.c buatlah log dengan format sebagai berikut.
DD-MM-YYYY HH:MM:SS ACCESSED [folder path]
DD-MM-YYYY HH:MM:SS MOVED [extension] file : [src path] > [folder dst]
DD-MM-YYYY HH:MM:SS MADE [folder name]
examples : 
02-05-2023 10:01:02 ACCESSED files
02-05-2023 10:01:03 ACCESSED files/abcd
02-05-2023 10:01:04 MADE categorized
02-05-2023 10:01:05 MADE categorized/jpg
02-05-2023 10:01:06 MOVED jpg file : files/abcd/foto.jpg > categorized/jpg
Catatan:
Path dimulai dari folder files atau categorized
Simpan di dalam log.txt
ACCESSED merupakan folder files beserta dalamnya
Urutan log tidak harus sama
Untuk mengecek apakah log-nya benar, buatlah suatu program baru dengan nama logchecker.c untuk mengekstrak informasi dari log.txt dengan ketentuan sebagai berikut.
Untuk menghitung banyaknya ACCESSED yang dilakukan.
Untuk membuat list seluruh folder yang telah dibuat dan banyaknya file yang dikumpulkan ke folder tersebut, terurut secara ascending.
Untuk menghitung banyaknya total file tiap extension, terurut secara ascending.


### Pengerjaan dan Pembahasan
- dikerjakan oleh: Satria Surya Prana (5025211073)
```
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <dirent.h>
#include <string.h>
#include <fcntl.h>

void downloadzip(char url[], char namafile[]){
if (fork()==0){
   char *argv[]={"wget", "--no-check-certificate", "-O", namafile, url, NULL};
   execv("/bin/wget", argv);
   exit(EXIT_SUCCESS);
}
int status;
while(wait(&status)>=0);
return;
}
void extract(char namafile[]){
if (fork()==0){
   char *argv[]={"unzip", namafile, NULL};
   execv("/usr/bin/unzip", argv);
   exit(EXIT_SUCCESS);
}
int status;
while(wait(&status)>=0);
return;
}

int main(){
downloadzip("https://drive.google.com/uc?export=download&id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp", "hehe.zip");
extract("hehe.zip");
}

```
```
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <sys/stat.h>
#include <pthread.h>
#include <errno.h>
#include <time.h>

#define MAX_PATH_LENGTH 1024
#define MAX_EXT_LENGTH 64
#define MAX_THREAD_COUNT 100
#define MAX_LINE_LENGTH 512

typedef struct {
    char src[MAX_PATH_LENGTH];
    char dst[MAX_PATH_LENGTH];
    char extension[MAX_EXT_LENGTH];
} FileMoveInfo;

typedef struct {
    char extension[MAX_EXT_LENGTH];
    int count;
    int max_files;
} ExtensionInfo;

char files_folder[MAX_PATH_LENGTH];
char categorized_folder[MAX_PATH_LENGTH];
char log_file[MAX_PATH_LENGTH];
ExtensionInfo extensions[MAX_EXT_LENGTH];
int ext_count = 0;

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

void log_accessed(char *path) {
    char timestamp[20];
    time_t t = time(NULL);
    strftime(timestamp, sizeof(timestamp), "%d-%m-%Y %H:%M:%S", localtime(&t));

    FILE *fp = fopen(log_file, "a");
    if (fp) {
        fprintf(fp, "%s ACCESSED [%s]\n", timestamp, path);
        fclose(fp);
    } else {
        printf("Failed to write to log file: %s\n", strerror(errno));
    }
}

void log_moved(FileMoveInfo *info) {
    char timestamp[20];
    time_t t = time(NULL);
    strftime(timestamp, sizeof(timestamp), "%d-%m-%Y %H:%M:%S", localtime(&t));

    FILE *fp = fopen(log_file, "a");
    if (fp) {
        fprintf(fp, "%s MOVED [%s] file : [%s] > [%s]\n", timestamp, info->extension, info->src, info->dst);
        fclose(fp);
    } else {
        printf("Failed to write to log file: %s\n", strerror(errno));
    }
}

void log_made(char *folder_name) {
    char timestamp[20];
    time_t t = time(NULL);
    strftime(timestamp, sizeof(timestamp), "%d-%m-%Y %H:%M:%S", localtime(&t));

    FILE *fp = fopen(log_file, "a");
    if (fp) {
        fprintf(fp, "%s MADE [%s]\n", timestamp, folder_name);
        fclose(fp);
    } else {
        printf("Failed to write to log file: %s\n", strerror(errno));
    }
}

void get_extensions() {
    FILE *fp = fopen("extensions.txt", "r");
    if (!fp) {
        printf("Failed to open extensions.txt: %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    }

    char line[MAX_LINE_LENGTH];
    while (fgets(line, MAX_LINE_LENGTH, fp)) {
        char *ext = strtok(line, "\n");
        strncpy(extensions[ext_count].extension, ext, MAX_EXT_LENGTH);
        extensions[ext_count].count = 0;
        extensions[ext_count].max_files = 0;
        ext_count++;
    }

    fclose(fp);
}

int is_valid_extension(char *filename, char **valid_extensions, int num_extensions) {
    int len = strlen(filename);
    int i, j;
    for (i = len - 1; i >= 0; i--) {
        if (filename[i] == '.') {
            // found the extension separator
            char *extension = &filename[i + 1];
            for (j = 0; j < num_extensions; j++) {
                if (strcasecmp(extension, valid_extensions[j]) == 0) {
                    // found a valid extension
                    return j;
                }
            }
            // the extension is not in the list of valid extensions
            return -1;
        }
    }
    // there is no extension in the filename
    return -1;
}


void *copyFile(void *arg) {
    struct thread_args *args = arg;
    char *filename = args->filename;
    char *folder_dst = args->folder_dst;
    char *log_path = args->log_path;

    // Check if file extension is in the list
    char *ext = getExtension(filename);
    if (isExtensionExist(ext)) {
        char *folder_name = getFolderName(ext);
        char *folder_path = getFolderPath(folder_dst, folder_name);

        // Create the folder if it doesn't exist
        createFolderIfNotExist(folder_path, log_path);

        // Get maximum file count
        int max_file_count = getMaxFileCount(ext);

        // Create new folder if the current folder is full
        if (isFolderFull(folder_path, max_file_count)) {
            char *new_folder_name = getNewFolderName(folder_name, folder_dst);
            char *new_folder_path = getFolderPath(folder_dst, new_folder_name);
            createFolderIfNotExist(new_folder_path, log_path);
            folder_path = new_folder_path;
        }

        // Copy the file to the destination folder
        char *src_path = getFilePath(args->folder_src, filename);
        char *dst_path = getFilePath(folder_path, filename);
        moveFile(src_path, dst_path, log_path);

        free(ext);
        free(folder_name);
        free(folder_path);
    } else {
        // Copy the file to the other folder
        char *src_path = getFilePath(args->folder_src, filename);
        char *dst_path = getFilePath(getOtherFolderPath(folder_dst), filename);
        moveFile(src_path, dst_path, log_path);

        free(ext);
    }

    free(filename);
    pthread_exit(NULL);
}

void categorize(char *folder_src, char *folder_dst, char *log_path) {
    // Create the categorized folder if it doesn't exist
    createFolderIfNotExist(folder_dst, log_path);

    // Get the list of files in the source folder
    char **files = getFileList(folder_src);
    int file_count = getFileCount(folder_src);

    // Create threads to copy each file
    pthread_t threads[file_count];
    int thread_count = 0;

    for (int i = 0; i < file_count; i++) {
        struct thread_args *args = malloc(sizeof(struct thread_args));
        args->filename = files[i];
        args->folder_src = folder_src;
        args->folder_dst = folder_dst;
        args->log_path = log_path;

        pthread_create(&threads[thread_count], NULL, copyFile, args);
        thread_count++;
    }

    // Wait for all threads to finish
    for (int i = 0; i < thread_count; i++) {
        pthread_join(threads[i], NULL);
    }

    // Free memory
    free(files);
}

int main() {
    char *extensions_path = "files/extensions.txt";
    char *max_path = "files/max.txt";
    char *folder_src = "files";
    char *folder_dst = "categorized";
    char *log_path = "log.txt";

    // Load extension list
    loadExtensions(extensions_path);

    // Load max file count
    loadMaxFileCount(max_path);

    // Categorize files
    categorize(folder_src, folder_dst, log_path);

    // Print file counts
    printFileCounts(folder_dst, log_path);

    return 0;
}

```
