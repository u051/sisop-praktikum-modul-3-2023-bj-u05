#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>

// Node untuk Huffman tree
struct huff_node {
    unsigned char c;
    int freq;
    struct huff_node *left, *right;
};

// Array untuk menyimpan frekuensi kemunculan tiap huruf
int freq[256];

// Fungsi untuk menghitung frekuensi kemunculan tiap huruf pada file
void count_freq(char *filename) {
    int fd = open(filename, O_RDONLY);
    if (fd < 0) {
        perror("Error opening file");
        exit(1);
    }
    unsigned char buf;
    while (read(fd, &buf, 1) > 0) {
        freq[buf]++;
    }
    close(fd);
}

// Fungsi untuk menghapus Huffman tree
void free_tree(struct huff_node *root) {
    if (root != NULL) {
        free_tree(root->left);
        free_tree(root->right);
        free(root);
    }
}

// Fungsi untuk membangun Huffman tree
struct huff_node *build_tree() {
    struct huff_node *nodes[256];
    int n = 0;
    for (int i = 0; i < 256; i++) {
        if (freq[i] > 0) {
            nodes[n] = malloc(sizeof(struct huff_node));
            nodes[n]->c = (unsigned char) i;
            nodes[n]->freq = freq[i];
            nodes[n]->left = NULL;
            nodes[n]->right = NULL;
            n++;
        }
    }
    while (n > 1) {
        struct huff_node *min1 = nodes[0], *min2 = nodes[1];
        int idx1 = 0, idx2 = 1;
        for (int i = 2; i < n; i++) {
            if (nodes[i]->freq < min1->freq) {
                min2 = min1;
                idx2 = idx1;
                min1 = nodes[i];
                idx1 = i;
            } else if (nodes[i]->freq < min2->freq) {
                min2 = nodes[i];
                idx2 = i;
            }
        }
        struct huff_node *new_node = malloc(sizeof(struct huff_node));
        new_node->c = 0;
        new_node->freq = min1->freq + min2->freq;
        new_node->left = min1;
        new_node->right = min2;
        nodes[idx1] = new_node;
        nodes[idx2] = nodes[n-1];
        n--;
    }
    return nodes[0];
}

// Fungsi rekursif untuk menulis kode Huffman tiap huruf ke file output
void write_codes(struct huff_node *node, unsigned char *code, int len, int fd) {
    if (node->left == NULL && node->right == NULL) {
        unsigned char buf[2] = {node->c, len};
        write(fd, buf, 2);
        write(fd, code, len);
    } else {
        code[len] = 0;
        write_codes(node->left, code, len+1, fd);
        code[len] = 1;
        write_codes(node->right, code, len+1, fd);
    }
}
