#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <sys/stat.h>
#include <pthread.h>
#include <errno.h>
#include <time.h>

#define MAX_PATH_LENGTH 1024
#define MAX_EXT_LENGTH 64
#define MAX_THREAD_COUNT 100
#define MAX_LINE_LENGTH 512

typedef struct {
    char src[MAX_PATH_LENGTH];
    char dst[MAX_PATH_LENGTH];
    char extension[MAX_EXT_LENGTH];
} FileMoveInfo;

typedef struct {
    char extension[MAX_EXT_LENGTH];
    int count;
    int max_files;
} ExtensionInfo;

char files_folder[MAX_PATH_LENGTH];
char categorized_folder[MAX_PATH_LENGTH];
char log_file[MAX_PATH_LENGTH];
ExtensionInfo extensions[MAX_EXT_LENGTH];
int ext_count = 0;

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

void log_accessed(char *path) {
    char timestamp[20];
    time_t t = time(NULL);
    strftime(timestamp, sizeof(timestamp), "%d-%m-%Y %H:%M:%S", localtime(&t));

    FILE *fp = fopen(log_file, "a");
    if (fp) {
        fprintf(fp, "%s ACCESSED [%s]\n", timestamp, path);
        fclose(fp);
    } else {
        printf("Failed to write to log file: %s\n", strerror(errno));
    }
}

void log_moved(FileMoveInfo *info) {
    char timestamp[20];
    time_t t = time(NULL);
    strftime(timestamp, sizeof(timestamp), "%d-%m-%Y %H:%M:%S", localtime(&t));

    FILE *fp = fopen(log_file, "a");
    if (fp) {
        fprintf(fp, "%s MOVED [%s] file : [%s] > [%s]\n", timestamp, info->extension, info->src, info->dst);
        fclose(fp);
    } else {
        printf("Failed to write to log file: %s\n", strerror(errno));
    }
}

void log_made(char *folder_name) {
    char timestamp[20];
    time_t t = time(NULL);
    strftime(timestamp, sizeof(timestamp), "%d-%m-%Y %H:%M:%S", localtime(&t));

    FILE *fp = fopen(log_file, "a");
    if (fp) {
        fprintf(fp, "%s MADE [%s]\n", timestamp, folder_name);
        fclose(fp);
    } else {
        printf("Failed to write to log file: %s\n", strerror(errno));
    }
}

void get_extensions() {
    FILE *fp = fopen("extensions.txt", "r");
    if (!fp) {
        printf("Failed to open extensions.txt: %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    }

    char line[MAX_LINE_LENGTH];
    while (fgets(line, MAX_LINE_LENGTH, fp)) {
        char *ext = strtok(line, "\n");
        strncpy(extensions[ext_count].extension, ext, MAX_EXT_LENGTH);
        extensions[ext_count].count = 0;
        extensions[ext_count].max_files = 0;
        ext_count++;
    }

    fclose(fp);
}

int is_valid_extension(char *filename, char **valid_extensions, int num_extensions) {
    int len = strlen(filename);
    int i, j;
    for (i = len - 1; i >= 0; i--) {
        if (filename[i] == '.') {
            // found the extension separator
            char *extension = &filename[i + 1];
            for (j = 0; j < num_extensions; j++) {
                if (strcasecmp(extension, valid_extensions[j]) == 0) {
                    // found a valid extension
                    return j;
                }
            }
            // the extension is not in the list of valid extensions
            return -1;
        }
    }
    // there is no extension in the filename
    return -1;
}


void *copyFile(void *arg) {
    struct thread_args *args = arg;
    char *filename = args->filename;
    char *folder_dst = args->folder_dst;
    char *log_path = args->log_path;

    // Check if file extension is in the list
    char *ext = getExtension(filename);
    if (isExtensionExist(ext)) {
        char *folder_name = getFolderName(ext);
        char *folder_path = getFolderPath(folder_dst, folder_name);

        // Create the folder if it doesn't exist
        createFolderIfNotExist(folder_path, log_path);

        // Get maximum file count
        int max_file_count = getMaxFileCount(ext);

        // Create new folder if the current folder is full
        if (isFolderFull(folder_path, max_file_count)) {
            char *new_folder_name = getNewFolderName(folder_name, folder_dst);
            char *new_folder_path = getFolderPath(folder_dst, new_folder_name);
            createFolderIfNotExist(new_folder_path, log_path);
            folder_path = new_folder_path;
        }

        // Copy the file to the destination folder
        char *src_path = getFilePath(args->folder_src, filename);
        char *dst_path = getFilePath(folder_path, filename);
        moveFile(src_path, dst_path, log_path);

        free(ext);
        free(folder_name);
        free(folder_path);
    } else {
        // Copy the file to the other folder
        char *src_path = getFilePath(args->folder_src, filename);
        char *dst_path = getFilePath(getOtherFolderPath(folder_dst), filename);
        moveFile(src_path, dst_path, log_path);

        free(ext);
    }

    free(filename);
    pthread_exit(NULL);
}

void categorize(char *folder_src, char *folder_dst, char *log_path) {
    // Create the categorized folder if it doesn't exist
    createFolderIfNotExist(folder_dst, log_path);

    // Get the list of files in the source folder
    char **files = getFileList(folder_src);
    int file_count = getFileCount(folder_src);

    // Create threads to copy each file
    pthread_t threads[file_count];
    int thread_count = 0;

    for (int i = 0; i < file_count; i++) {
        struct thread_args *args = malloc(sizeof(struct thread_args));
        args->filename = files[i];
        args->folder_src = folder_src;
        args->folder_dst = folder_dst;
        args->log_path = log_path;

        pthread_create(&threads[thread_count], NULL, copyFile, args);
        thread_count++;
    }

    // Wait for all threads to finish
    for (int i = 0; i < thread_count; i++) {
        pthread_join(threads[i], NULL);
    }

    // Free memory
    free(files);
}

int main() {
    char *extensions_path = "files/extensions.txt";
    char *max_path = "files/max.txt";
    char *folder_src = "files";
    char *folder_dst = "categorized";
    char *log_path = "log.txt";

    // Load extension list
    loadExtensions(extensions_path);

    // Load max file count
    loadMaxFileCount(max_path);

    // Categorize files
    categorize(folder_src, folder_dst, log_path);

    // Print file counts
    printFileCounts(folder_dst, log_path);

    return 0;
}
