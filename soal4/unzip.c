#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <dirent.h>
#include <string.h>
#include <fcntl.h>

void downloadzip(char url[], char namafile[]){
if (fork()==0){
   char *argv[]={"wget", "--no-check-certificate", "-O", namafile, url, NULL};
   execv("/bin/wget", argv);
   exit(EXIT_SUCCESS);
}
int status;
while(wait(&status)>=0);
return;
}
void extract(char namafile[]){
if (fork()==0){
   char *argv[]={"unzip", namafile, NULL};
   execv("/usr/bin/unzip", argv);
   exit(EXIT_SUCCESS);
}
int status;
while(wait(&status)>=0);
return;
}

int main(){
downloadzip("https://drive.google.com/uc?export=download&id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp", "hehe.zip");
extract("hehe.zip");
}
